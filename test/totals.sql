-- Check totals:
-- This test check if we have the same total amount of payment
-- between the STG.fact_payments_tmp (without duplications) and DWH.fact_payments
-- Assumption - we have one load a day (that's why I am slicing the dw_update_date of the
-- DWH table with today's date)
-- ===============================================================================

SELECT DISTINCT *
FROM (  SELECT SUM(payment_amount) FROM DWH.fact_payments WHERE DATEDIFF('day', dw_update_date, SYSDATE()) = 0
        UNION ALL
        SELECT SUM(payment_amount) FROM STG.fact_payments_tmp) as CH ;