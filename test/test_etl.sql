-- Step 1 - create a new data for testing our etl process
-- ==========================================================================================================
-- ==========================================================================================================
CREATE TABLE IF NOT EXISTS ODS.new_payments like ODS.Payments;

CREATE TABLE IF NOT EXISTS ODS.new_merchants like ODS.Merchants;

TRUNCATE TABLE ODS.new_payments;

TRUNCATE TABLE ODS.new_merchants;

INSERT INTO ODS.new_merchants
SELECT * FROM ODS.Merchants;

-- Test 1 - check if we removing a duplicated payment with different payment_date
-- ----------------------------------------------------------------------------------------------------------
INSERT INTO ODS.new_payments
(payment_id, merchant_id, payment_amount, payment_method, status, payment_date, customer_id, ingestion_date)
VALUES(1, 'aaaa', 111.00, 'Visa', 'Paid', '2019-03-22 00:00:00', 'zzzz', '2019-03-22');

INSERT INTO ODS.new_payments
(payment_id, merchant_id, payment_amount, payment_method, status, payment_date, customer_id, ingestion_date)
VALUES(1, 'aaaa', 111.00, 'Visa', 'Pending', '2019-03-19 00:00:00', 'zzzz', '2019-03-19');


-- Test 2 - check if we replacing NULL values properly
-- ----------------------------------------------------------------------------------------------------------
INSERT INTO ODS.new_payments
(payment_id, merchant_id, payment_amount, payment_method, status, payment_date, customer_id, ingestion_date)
VALUES(2, NULL , NULL, NULL, NULL, NULL, NULL, NULL);


-- Test 3 - Check if we were able to successfully insert a new Payment record to the DWH
-- ----------------------------------------------------------------------------------------------------------
INSERT INTO ODS.new_payments
(payment_id, merchant_id, payment_amount, payment_method, status, payment_date, customer_id, ingestion_date)
VALUES(3, 'bbbb', 222.00, 'Mastercard', 'Pending', '2019-03-17 00:00:00', 'xxxx', '2019-03-17');


-- Test 4 - Check if we were able to successfully update an old record that already exists in the DWH

-- Current payment in DWH:
-- =======================
-- F.payment_id,             -- expected -> 7660790
-- F.merchant_key,           -- expected -> 88
-- M.merchant_id,            -- expected -> 'd08f2adc-394d-47c6-9353-741683aa3bba'
-- F.payment_amount,         -- expected -> '117.5500'
-- F.payment_method_key,     -- expected -> 4
-- PM.payment_method,        -- expected -> 'Visa'
-- F.payment_status_key,     -- expected -> 4
-- PS.payment_status,        -- expected -> 'Rejected'
-- F.payment_date_key,       -- expected -> '20180617'
-- T.day_of_week,            -- expected -> 'Sunday'
-- F.customer_id,            -- expected -> '626c4fca-7eb5-4e00-a427-be94949da7c8'
-- F.ingestion_date          -- expected -> '2018-01-16'
-- ----------------------------------------------------------------------------------------------------------
INSERT INTO ODS.new_payments
(payment_id, merchant_id, payment_amount, payment_method, status, payment_date, customer_id, ingestion_date)
VALUES(7660790, 'd08f2adc-394d-47c6-9353-741683aa3bba', 130.00, 'Mastercard', 'Pending', '2019-03-22 00:00:00', '626c4fca-7eb5-4e00-a427-be94949da7c8', '2019-03-22');


-- Test 5 - Check RI for dim's
-- ----------------------------------------------------------------------------------------------------------
INSERT INTO ODS.new_payments
(payment_id, merchant_id, payment_amount, payment_method, status, payment_date, customer_id, ingestion_date)
VALUES(5, 'eeee', 555.00, 'BitCoin', 'Transfer', '2019-03-22 00:00:00', 'yyyy', '2019-03-22');


-- Test 6 - Check if we were able to successfully insert a new Merchant record to the DWH
-- ----------------------------------------------------------------------------------------------------------
INSERT INTO ODS.new_merchants
(merchant_id, merchant_name, address, phone_number, email, ingestion_date)
VALUES('ffff', 'FizBiz', '123 Lalaland st.', '123-456-789', 'FizBiz@FizBiz.com', '2019-03-22');


-- Test 7 - Check if we were able to successfully update an old Merchant record that already exists in the DWH

-- Current merchant in DWH:
-- =======================
-- merchant_key,   -- expected -> 2
-- merchant_id,    -- expected -> '015dc9b6-269c-405b-b47f-f0c52445169f'
-- merchant_name,  -- expected -> 'Realcube'
-- address,        -- expected -> '872 Old Shore Lane'
-- phone_number,   -- expected -> '388-168-9899'
-- email,          -- expected -> 'lbanes1s@hp.com'
-- ingestion_date, -- expected -> '2018-01-17'
-- dw_create_date, -- expected -> 2019-03-22
-- dw_update_date, -- expected -> 2019-03-22
-- ri_ind          -- expected -> 0
-- ----------------------------------------------------------------------------------------------------------
UPDATE ODS.new_merchants
SET merchant_name = 'FizBiz Company',
    address       = '111 Lafland st.',
    phone_number  = '111-111-111',
    email         = 'FizBizCompany@FizBiz.com',
    ingestion_date= '2019-03-22'
WHERE merchant_id = '015dc9b6-269c-405b-b47f-f0c52445169f';


-- Test 8 - check if we replacing NULL values properly
-- ----------------------------------------------------------------------------------------------------------
INSERT INTO ODS.new_merchants
(merchant_id, merchant_name, address, phone_number, email, ingestion_date)
VALUES('gggg', NULL , NULL, NULL, NULL, NULL);



SELECT COUNT(*) FROM ODS.new_payments;
SELECT COUNT(*) FROM ODS.new_merchants;  -- expected -> 205 V

COMMIT;

-- Step 2 - Swap Tables replace ODS.Payments with ODS.new_payments AND replace ODS.Merchants with ODS.new_merchants
-- ==========================================================================================================
-- ==========================================================================================================

-- Swap Tables
-- ODS.Payments to ODS.tmp_Payments
-- ODS.new_payments to ODS.Payments
-- ODS.tmp_Payments to ODS.new_payments
SET SEARCH_PATH = 'ODS';
ALTER TABLE Payments, new_payments, tmp_Payments RENAME TO tmp_Payments, Payments, new_payments;


SELECT COUNT(*) FROM ODS.Payments;  -- expected -> 6 V

-- Swap Tables
-- ODS.Merchants to ODS.tmp_Merchants
-- ODS.new_merchants to ODS.Merchants
-- ODS.tmp_Merchants to ODS.new_merchants
SET SEARCH_PATH = 'ODS';
ALTER TABLE Merchants, new_merchants, tmp_Merchants RENAME TO tmp_Merchants, Merchants, new_merchants;


SELECT COUNT(*) FROM ODS.Merchants;  -- expected -> 205 V

COMMIT;

-- Step 3 - Run ETL
-- ==========================================================================================================
-- ==========================================================================================================


-- Step 4 - Run next SQL queries:
-- ==========================================================================================================
-- ==========================================================================================================

-- Test 1 expected result one row for payment_id = 1 in STG.fact_payments_tmp with payment_date =  2019-03-22
-- ----------------------------------------------------------------------------------------------------------
SELECT * FROM STG.fact_payments_tmp WHERE payment_id = 1; -- V Checked


-- Test 2 expected result one row for payment_id = 2 in STG.fact_payments_tmp no NULL values
-- ----------------------------------------------------------------------------------------------------------
SELECT * FROM STG.fact_payments_tmp WHERE payment_id = 2; -- V Checked


-- Test 3 expected result one row for payment_id = 3 in DWH.fact_payments  -- V Checked
-- This also testing for proper join to al dim's
-- check if another partition created for month - 201903 with (select * from partitions;)
-- ----------------------------------------------------------------------------------------------------------
SELECT  F.payment_id,             -- expected -> 3
        F.merchant_key,           -- expected ->
        M.merchant_id,            -- expected -> 'bbbb'
        F.payment_amount,         -- expected -> '222.00'
        F.payment_method_key,     -- expected ->
        PM.payment_method,        -- expected -> 'Mastercard'
        F.payment_status_key,     -- expected ->
        PS.payment_status,        -- expected -> 'Pending'
        F.payment_date_key,       -- expected -> '20190317'
        T.day_of_week,            -- expected -> 'Sunday'
        F.customer_id,            -- expected -> 'xxxx'
        F.ingestion_date          -- expected -> '2019-03-17'
FROM    DWH.fact_payments AS F
        JOIN DWH.dim_merchants AS M
        ON F.merchant_key = M.merchant_key
        JOIN DWH.dim_payment_method AS PM
        ON F.payment_method_key = PM.payment_method_key
        JOIN DWH.dim_payment_status AS PS
        ON F.payment_status_key = PS.payment_status_key
        JOIN DWH.dim_time AS T
        ON F.payment_date_key = T.date_key
WHERE   payment_id = 3;

SELECT * FROM PARTITIONS; -- V Checked


-- Test 4 expected result one row for payment_id = 7660790 in DWH.fact_payments  -- V Checked
-- This also testing for proper join to all dim's

-- Updated expected payment:
-- =========================
-- F.payment_id,             -- expected -> 7660790
-- F.merchant_key,           -- expected -> 88
-- M.merchant_id,            -- expected -> 'd08f2adc-394d-47c6-9353-741683aa3bba'
-- F.payment_amount,         -- expected -> '130.00' (117.55 + fee's)
-- F.payment_method_key,     -- expected ->
-- PM.payment_method,        -- expected -> 'Mastercard' (new card)
-- F.payment_status_key,     -- expected ->
-- PS.payment_status,        -- expected -> 'Pending'
-- F.payment_date_key,       -- expected -> '20190322'
-- T.day_of_week,            -- expected -> 'Friday'
-- F.customer_id,            -- expected -> '626c4fca-7eb5-4e00-a427-be94949da7c8'
-- F.ingestion_date          -- expected -> '2019-03-22'
-- ----------------------------------------------------------------------------------------------------------

SELECT  F.payment_id,             -- expected -> 7660790
        F.merchant_key,           -- expected ->  88
        M.merchant_id,            -- expected -> 'd08f2adc-394d-47c6-9353-741683aa3bba'
        F.payment_amount,         -- expected -> '130.00'
        F.payment_method_key,     -- expected ->
        PM.payment_method,        -- expected -> 'Mastercard'
        F.payment_status_key,     -- expected ->
        PS.payment_status,        -- expected -> 'Pending'
        F.payment_date_key,       -- expected -> '20190322'
        T.day_of_week,            -- expected -> 'Friday'
        F.customer_id,            -- expected -> '626c4fca-7eb5-4e00-a427-be94949da7c8'
        F.ingestion_date          -- expected -> '2019-03-22'
FROM    DWH.fact_payments AS F
        JOIN DWH.dim_merchants AS M
          ON F.merchant_key = M.merchant_key
        JOIN DWH.dim_payment_method AS PM
          ON F.payment_method_key = PM.payment_method_key
        JOIN DWH.dim_payment_status AS PS
          ON F.payment_status_key = PS.payment_status_key
        JOIN DWH.dim_time AS T
          ON F.payment_date_key = T.date_key
WHERE   payment_id = 7660790;


-- Test 5 - check RI for dim's
-- ----------------------------------------------------------------------------------------------------------

-- dim_merchants -- V Checked

SELECT  *
FROM    DWH.dim_merchants
WHERE   merchant_id = 'eeee';


-- dim_payment_status -- V Checked

SELECT  *
FROM    DWH.dim_payment_status
WHERE   payment_status = 'Transfer';


-- dim_payment_method -- V Checked

SELECT  *
FROM    DWH.dim_payment_method
WHERE   payment_method = 'BitCoin';


-- Test 6 expected result - one row for merchant_id = 'ffff' in DWH.dim_merchants -- V Checked
-- ----------------------------------------------------------------------------------------------------------
SELECT  merchant_key,   -- expected ->
        merchant_id,    -- expected -> 'ffff'
        merchant_name,  -- expected -> 'FizBiz'
        address,        -- expected -> '123 Lalaland st.'
        phone_number,   -- expected -> '123-456-789'
        email,          -- expected -> 'FizBiz@FizBiz.com'
        ingestion_date, -- expected -> '2019-03-22'
        dw_create_date, -- expected -> now + few min
        dw_update_date, -- expected -> now + few min
        ri_ind          -- expected -> 0
FROM    DWH.dim_merchants
WHERE   merchant_id = 'ffff';


-- Test 7 expected result one row for merchant_id = 015dc9b6-269c-405b-b47f-f0c52445169f in DWH.dim_merchants -- V Checked

-- Updated expected payment:
-- =========================
-- merchant_key,   -- expected -> 2
-- merchant_id,    -- expected -> '015dc9b6-269c-405b-b47f-f0c52445169f'
-- merchant_name,  -- expected -> 'FizBiz Company'
-- address,        -- expected -> '111 Lafland st.'
-- phone_number,   -- expected -> '111-111-111'
-- email,          -- expected -> 'FizBizCompany@FizBiz.com'
-- ingestion_date, -- expected -> '2019-03-22'
-- dw_create_date, -- expected -> now + few min
-- dw_update_date, -- expected -> now + few min
-- ri_ind          -- expected -> 0
-- ----------------------------------------------------------------------------------------------------------
SELECT  merchant_key,   -- expected -> 2
        merchant_id,    -- expected -> '015dc9b6-269c-405b-b47f-f0c52445169f'
        merchant_name,  -- expected -> 'FizBiz Company'
        address,        -- expected -> '111 Lafland st.'
        phone_number,   -- expected -> '111-111-111'
        email,          -- expected -> 'FizBizCompany@FizBiz.com'
        ingestion_date, -- expected -> '2019-03-22'
        dw_create_date, -- expected -> now + few min
        dw_update_date, -- expected -> now + few min
        ri_ind          -- expected -> 0
FROM    DWH.dim_merchants
WHERE   merchant_id = '015dc9b6-269c-405b-b47f-f0c52445169f';

-- Test 8 - check if we replacing NULL values properly -- V Checked
-- ----------------------------------------------------------------------------------------------------------
SELECT  *
FROM    DWH.dim_merchants
WHERE   merchant_id = 'gggg';


-- Step 5 - CleanUP
-- ==========================================================================================================
-- ==========================================================================================================

-- Swap Tables back
-- ODS.Payments to ODS.tmp_Payments
-- ODS.new_payments to ODS.Payments
-- ODS.tmp_Payments to ODS.new_payments
-- ----------------------------------------------------------------------------------------------------------
SET SEARCH_PATH = 'ODS';
ALTER TABLE Payments, new_payments, tmp_Payments RENAME TO tmp_Payments, Payments, new_payments;


SELECT COUNT(*) FROM ODS.Payments;  -- expected -> 1601 V


-- Swap Tables
-- ODS.Merchants to ODS.tmp_Merchants
-- ODS.new_merchants to ODS.Merchants
-- ODS.tmp_Merchants to ODS.new_merchants
-- ----------------------------------------------------------------------------------------------------------
SET SEARCH_PATH = 'ODS';
ALTER TABLE Merchants, new_merchants, tmp_Merchants RENAME TO tmp_Merchants, Merchants, new_merchants;


SELECT COUNT(*) FROM ODS.Merchants;  -- expected -> 205 V

-- Drop test tables
-- ----------------------------------------------------------------------------------------------------------
SELECT COUNT(*) FROM ODS.new_payments; -- 6
SELECT COUNT(*) FROM ODS.new_merchants; -- 205


DROP TABLE ODS.new_payments;

DROP TABLE ODS.new_merchants;

COMMIT;