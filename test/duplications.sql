-- Check duplication:
-- This test check if we have duplications in the sk tables and the dimensions tables
-- ===================================================================================

-- dim_merchants

SELECT DISTINCT *
FROM (  SELECT COUNT(*) FROM DWH.dim_merchants
        UNION ALL
        SELECT COUNT(*) FROM ODS.dim_merchants_sk
        UNION ALL
        SELECT COUNT(DISTINCT merchant_id) FROM ODS.dim_merchants_sk  ) as CH ;

-- dim_payment_method

SELECT DISTINCT *
FROM (  SELECT COUNT(*) FROM DWH.dim_payment_method
        UNION ALL
        SELECT COUNT(*) FROM ODS.dim_payment_method_sk
        UNION ALL
        SELECT COUNT(DISTINCT payment_method) FROM ODS.dim_payment_method_sk  ) as CH ;

-- dim_payment_status

SELECT DISTINCT *
FROM (  SELECT COUNT(*) FROM DWH.dim_payment_status
        UNION ALL
        SELECT COUNT(*) FROM ODS.dim_payment_status_sk
        UNION ALL
        SELECT COUNT(DISTINCT payment_status) FROM ODS.dim_payment_status_sk  ) as CH ;

-- dim_time

SELECT DISTINCT *
FROM (  SELECT COUNT(*) FROM DWH.dim_time
        UNION ALL
        SELECT COUNT(DISTINCT date_key) FROM DWH.dim_time  ) as CH ;

-- fact_payments

SELECT DISTINCT *
FROM (  SELECT COUNT(*) FROM DWH.fact_payments
        UNION ALL
        SELECT COUNT(DISTINCT payment_id) FROM DWH.fact_payments  ) as CH ;

COMMIT;