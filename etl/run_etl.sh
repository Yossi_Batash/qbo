#!/bin/bash

INIT_DB=$1     #yes

TIME=$(date +%x_%H:%M:%S)



echo "$TIME--==Start ETL process ==================================--"

if [ "$INIT_DB" = "yes" ];
then
    echo "$TIME--==Step 0 - Initialize DWH =====+==================--"

    eval  "/opt/vertica/bin/vsql -f /home/dbadmin/qbo/database/create_dwh.sql -U dbadmin -w 123456"
fi

echo "$TIME--==Step 1 - Start loading dimensions ==================--"

eval  "/opt/vertica/bin/vsql -f /home/dbadmin/qbo/etl/sql/load_dims.sql -U dbadmin -w 123456"

echo "$TIME--==Step 2 - Start loading ODS.payments to STG =========--"

eval  "/opt/vertica/bin/vsql -f /home/dbadmin/qbo/etl/sql/load_stg.sql -U dbadmin -w 123456"

echo "$TIME--==Step 3 - Start RI process ==========================--"

eval  "/opt/vertica/bin/vsql -f /home/dbadmin/qbo/etl/sql/ri_dim_merchants.sql -U dbadmin -w 123456"
eval  "/opt/vertica/bin/vsql -f /home/dbadmin/qbo/etl/sql/ri_dim_payment_method.sql -U dbadmin -w 123456"
eval  "/opt/vertica/bin/vsql -f /home/dbadmin/qbo/etl/sql/ri_dim_payment_status.sql -U dbadmin -w 123456"

echo "$TIME--==Step 4 - Start Merge Fact process ==================--"

eval  "/opt/vertica/bin/vsql -f /home/dbadmin/qbo/etl/sql/merge_fact.sql -U dbadmin -w 123456"

echo "$TIME--==Step 5 - Start the data reliability tests ==========--"

echo "$TIME--==Check for duplications =============================--"

eval  "/opt/vertica/bin/vsql -f /home/dbadmin/qbo/test/duplications.sql -U dbadmin -w 123456"

echo "$TIME--==Check totals between STG to DWH ====================--"

eval  "/opt/vertica/bin/vsql -f /home/dbadmin/qbo/test/totals.sql -U dbadmin -w 123456"

echo "$TIME--==End ETL process ====================================--"