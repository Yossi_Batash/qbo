-- dim_merchants
-- ==============

-- Step 1 - Load STG.dim_merchants_tmp after Removing duplications from ODS.Merchants and replace NULL values
TRUNCATE TABLE STG.dim_merchants_tmp;

INSERT /*+ direct */ INTO STG.dim_merchants_tmp
WITH Mer
AS
( SELECT	merchant_id,
          merchant_name,
          address,
          phone_number,
          email,
          MAX(ingestion_date) AS ingestion_date
  FROM 	  ODS.Merchants
  GROUP BY 1,2,3,4,5 )

SELECT	merchant_id,
        IFNULL ( merchant_name  , 'Unknown value' ) 	AS merchant_name,
        IFNULL ( address 		    , 'Unknown value' ) 	AS address,
        IFNULL ( phone_number   , 'Unknown value' ) 	AS phone_number,
        IFNULL ( email 			    , 'Unknown value' ) 	AS email,
        IFNULL ( ingestion_date , '1900-01-01')       AS ingestion_date
FROM 	Mer;



-- Step 2 - Insert into SK

INSERT /*+ direct */ INTO ODS.dim_merchants_sk(merchant_id)
SELECT DISTINCT merchant_id
FROM  STG.dim_merchants_tmp AS a
WHERE NOT EXISTS (SELECT 1 FROM ODS.dim_merchants_sk AS b WHERE a.merchant_id = b.merchant_id);




-- Step 3 - Load STG.dim_merchants
TRUNCATE TABLE STG.dim_merchants;

INSERT /*+ direct */ INTO STG.dim_merchants
SELECT  sk.merchant_key     AS merchant_key,
        mwd.merchant_id     AS merchant_id,
        mwd.merchant_name   AS merchant_name,
        mwd.address         AS address,
        mwd.phone_number    AS phone_number,
        mwd.email           AS email,
        mwd.ingestion_date  AS ingestion_date
FROM    STG.dim_merchants_tmp AS mwd
        JOIN
        ODS.dim_merchants_sk AS sk
        ON mwd.merchant_id = sk.merchant_id;


-- Step 4 - Merge into DWH.dim_merchants

MERGE /*+ direct */ INTO DWH.dim_merchants AS dwh
USING STG.dim_merchants AS stg
ON dwh.merchant_key = stg.merchant_key
WHEN MATCHED
  THEN  UPDATE
        SET merchant_name  = stg.merchant_name,
            address        = stg.address,
            phone_number   = stg.phone_number,
            email          = stg.email,
            ingestion_date = stg.ingestion_date,
            dw_update_date = SYSDATE,
            ri_ind         = 0
WHEN NOT MATCHED
  THEN  INSERT (  merchant_key,
                  merchant_id,
                  merchant_name,
                  address,
                  phone_number,
                  email,
                  ingestion_date,
                  dw_create_date,
                  dw_update_date,
                  ri_ind)
  VALUES (stg.merchant_key,
          stg.merchant_id,
          stg.merchant_name,
          stg.address,
          stg.phone_number,
          stg.email,
          stg.ingestion_date,
          SYSDATE,
          SYSDATE,
          0);

COMMIT;