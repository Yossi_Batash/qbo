-- Step 1 - create stg
TRUNCATE TABLE STG.fact_payments;

INSERT INTO STG.fact_payments
SELECT  stg.payment_id          AS payment_id,
        m.merchant_key          AS merchant_key,
        stg.payment_amount      AS payment_amount,
        pm.payment_method_key   AS payment_method_key,
        ps.payment_status_key   AS payment_status_key,
        REPLACE(LEFT(CAST(stg.payment_date AS VARCHAR),10), '-', '')::INT AS payment_date_key,
        stg.customer_id         AS customer_id,
        stg.ingestion_date
FROM    STG.fact_payments_tmp AS stg
        JOIN
        ODS.dim_merchants_sk AS m
        ON stg.merchant_id = m.merchant_id
        JOIN
        ODS.dim_payment_method_sk AS pm
        ON stg.payment_method = pm.payment_method
        JOIN
        ODS.dim_payment_status_sk ps
        ON stg.payment_status = ps.payment_status;


-- Step 2 - Merge into DWH.fact_payments
MERGE /*+ direct */ INTO DWH.fact_payments AS dwh
USING STG.fact_payments AS stg

ON dwh.payment_id = stg.payment_id
WHEN MATCHED
THEN  UPDATE
  SET merchant_key       = stg.merchant_key,
      payment_amount     = stg.payment_amount,
      payment_method_key = stg.payment_method_key,
      payment_status_key = stg.payment_status_key,
      payment_date_key   = stg.payment_date_key,
      customer_id        = stg.customer_id,
      ingestion_date     = stg.ingestion_date,
      dw_update_date     = SYSDATE
WHEN NOT MATCHED
THEN  INSERT (  payment_id,
                merchant_key,
                payment_amount,
                payment_method_key,
                payment_status_key,
                payment_date_key,
                customer_id,
                ingestion_date,
                dw_create_date,
                dw_update_date)
VALUES (stg.payment_id,
        stg.merchant_key,
        stg.payment_amount,
        stg.payment_method_key,
        stg.payment_status_key,
        stg.payment_date_key,
        stg.customer_id,
        stg.ingestion_date,
        SYSDATE,
        SYSDATE);

COMMIT;