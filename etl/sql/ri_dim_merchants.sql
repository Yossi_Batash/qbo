-- ODS.dim_merchants_sk
INSERT /*+ direct */ INTO ODS.dim_merchants_sk(merchant_id)
SELECT DISTINCT merchant_id
FROM  STG.fact_payments_tmp AS a
WHERE NOT EXISTS (SELECT 1 FROM ODS.dim_merchants_sk AS b WHERE a.merchant_id = b.merchant_id);

-- DWH.dim_merchants
insert into DWH.dim_merchants(merchant_key,
                              merchant_id,
                              merchant_name,
                              address,
                              phone_number,
                              email,
                              ingestion_date,
                              dw_create_date,
                              dw_update_date,
                              ri_ind)
  SELECT  merchant_key,
          merchant_id,
          'Unknown value' AS merchant_name,
          'Unknown value' AS address,
          'Unknown value' AS phone_number,
          'Unknown value' AS email,
          '1900-01-01'    AS ingestion_date,
          sysdate,
          sysdate,
          1
  FROM (
         SELECT DISTINCT  merchant_key,
                          merchant_id
         FROM ODS.dim_merchants_sk stg
         where not exists
         (
             SELECT 1
             FROM   DWH.dim_merchants dw
             WHERE  stg.merchant_key = dw.merchant_key
         )
       ) AS N;

COMMIT;