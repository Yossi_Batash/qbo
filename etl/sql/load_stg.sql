-- Load ODS.merchants_without_dup after Removing duplications from ODS.Merchants and replace NULL values
TRUNCATE TABLE STG.fact_payments_tmp;

INSERT /*+ direct */ INTO STG.fact_payments_tmp
WITH Pay
AS
(
  SELECT  payment_id    ,
          merchant_id   ,
          payment_amount,
          payment_method,
          status        AS payment_status,
          payment_date  ,
          customer_id   ,
          ingestion_date,
          RANK () OVER (PARTITION BY payment_id ORDER BY payment_date desc) AS rank
  FROM 	  ODS.Payments )

SELECT	payment_id,
        IFNULL ( merchant_id    , 'Unknown value' ) 	AS merchant_id,
        IFNULL ( payment_amount , 0 ) 	              AS payment_amount,
        IFNULL ( payment_method , 'Unknown value' ) 	AS payment_method,
        IFNULL ( payment_status , 'Unknown value' ) 	AS payment_status,
        IFNULL ( payment_date   , '1900-01-01' ) 	    AS payment_date,
        IFNULL ( customer_id    , 'Unknown value' ) 	AS customer_id,
        IFNULL ( ingestion_date , '1900-01-01') AS ingestion_date
FROM 	Pay
WHERE  rank = 1;

COMMIT;

SELECT ANALYZE_STATISTICS('STG.fact_payments_tmp');

