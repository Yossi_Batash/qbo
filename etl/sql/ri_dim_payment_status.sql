-- ODS.dim_payment_status_sk
INSERT /*+ direct */ INTO ODS.dim_payment_status_sk(payment_status)
  SELECT DISTINCT payment_status
  FROM  STG.fact_payments_tmp AS a
  WHERE NOT EXISTS (SELECT 1 FROM ODS.dim_payment_status_sk AS b WHERE a.payment_status = b.payment_status);

-- DWH.dim_payment_status
insert into DWH.dim_payment_status( payment_status_key,
                                    payment_status,
                                    dw_create_date,
                                    dw_update_date,
                                    ri_ind)
SELECT  payment_status_key,
        payment_status,
        sysdate,
        sysdate,
        1
FROM (
       SELECT DISTINCT  payment_status_key,
                        payment_status
       FROM ODS.dim_payment_status_sk stg
       where not exists
       (
           SELECT 1
           FROM   DWH.dim_payment_status dw
           WHERE  stg.payment_status_key = dw.payment_status_key
       )
     ) AS N;

COMMIT;