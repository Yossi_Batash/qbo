-- ODS.dim_payment_method_sk
INSERT /*+ direct */ INTO ODS.dim_payment_method_sk(payment_method)
  SELECT DISTINCT payment_method
  FROM  STG.fact_payments_tmp AS a
  WHERE NOT EXISTS (SELECT 1 FROM ODS.dim_payment_method_sk AS b WHERE a.payment_method = b.payment_method);

-- DWH.dim_payment_method
insert into DWH.dim_payment_method(payment_method_key,
                                   payment_method,
                                   dw_create_date,
                                   dw_update_date,
                                   ri_ind)
  SELECT  payment_method_key,
          payment_method,
          sysdate,
          sysdate,
          1
  FROM (
         SELECT DISTINCT  payment_method_key,
                          payment_method
         FROM ODS.dim_payment_method_sk stg
         where not exists
         (
             SELECT 1
             FROM   DWH.dim_payment_method dw
             WHERE  stg.payment_method_key = dw.payment_method_key
         )
       ) AS N;
COMMIT;