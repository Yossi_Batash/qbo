-- Remove all objects
-- --------------------

-- Drop all ODS tables
DROP TABLE IF EXISTS ODS.dim_merchants_sk       CASCADE;
DROP TABLE IF EXISTS ODS.dim_payment_method_sk  CASCADE;
DROP TABLE IF EXISTS ODS.dim_payment_status_sk  CASCADE;
-- Drop all seq
DROP SEQUENCE IF EXISTS ODS.seq_dim_merchants_sk;
DROP SEQUENCE IF EXISTS ODS.seq_dim_payment_method_sk;
DROP SEQUENCE IF EXISTS ODS.seq_dim_payment_status_sk;
-- Drop all STG tables
DROP TABLE IF EXISTS STG.dim_merchants_tmp      CASCADE;
DROP TABLE IF EXISTS STG.dim_merchants          CASCADE;
DROP TABLE IF EXISTS STG.fact_payments          CASCADE;
DROP TABLE IF EXISTS STG.fact_payments_tmp      CASCADE;
-- Drop all DWH tables
DROP TABLE IF EXISTS DWH.dim_time               CASCADE;
DROP TABLE IF EXISTS DWH.dim_merchants          CASCADE;
DROP TABLE IF EXISTS DWH.dim_payment_status     CASCADE;
DROP TABLE IF EXISTS DWH.dim_payment_method     CASCADE;
DROP TABLE IF EXISTS DWH.fact_payments          CASCADE;
-- Drop all schema
DROP SCHEMA IF EXISTS STG;
DROP SCHEMA IF EXISTS DWH;


-- ODS
-- ####

-- ODS.dim_merchants_sk
-- -----------------------

CREATE SEQUENCE ODS.seq_dim_merchants_sk START WITH 2;

CREATE TABLE IF NOT EXISTS ODS.dim_merchants_sk
(
  merchant_key      INT DEFAULT NEXTVAL('ODS.seq_dim_merchants_sk') PRIMARY KEY,
  merchant_id       CHAR(36)
)
UNSEGMENTED ALL NODES;

INSERT INTO ODS.dim_merchants_sk (merchant_key, merchant_id) VALUES(1, 'Unknown value');

-- SELECT * FROM ODS.dim_merchants_sk;

-- ODS.dim_payment_method_sk
-- -----------------------

CREATE SEQUENCE ODS.seq_dim_payment_method_sk START WITH 2;

CREATE TABLE IF NOT EXISTS ODS.dim_payment_method_sk
(
  payment_method_key  INT DEFAULT NEXTVAL('ODS.seq_dim_payment_method_sk') PRIMARY KEY,
  payment_method      VARCHAR(50)
)
UNSEGMENTED ALL NODES;

INSERT INTO ODS.dim_payment_method_sk (payment_method_Key, payment_method) VALUES(1, 'Unknown value');

-- SELECT * FROM ODS.dim_payment_method_sk;

-- ODS.dim_payment_status_sk
-- -----------------------

CREATE SEQUENCE ODS.seq_dim_payment_status_sk START WITH 2;

CREATE TABLE IF NOT EXISTS ODS.dim_payment_status_sk
(
  payment_status_key  INT DEFAULT NEXTVAL('ODS.seq_dim_payment_status_sk') PRIMARY KEY,
  payment_status  		VARCHAR(20)
)
UNSEGMENTED ALL NODES;

INSERT INTO ODS.dim_payment_status_sk(payment_status_key, payment_status) VALUES(1, 'Unknown value');

-- SELECT * FROM ODS.dim_payment_status_sk;

-- STG
-- #####

CREATE SCHEMA IF NOT EXISTS STG;

-- STG.dim_merchants_tmp
-- -----------------------

CREATE TABLE IF NOT EXISTS STG.dim_merchants_tmp
(
  merchant_id    	 CHAR(36),
  merchant_name  	 VARCHAR(150),
  address        	 VARCHAR(250),
  phone_number   	 VARCHAR(20),
  email          	 VARCHAR(100),
  ingestion_date 	 DATE
)
UNSEGMENTED ALL NODES;

-- STG.dim_merchants
-- -----------------------

CREATE TABLE IF NOT EXISTS STG.dim_merchants
(
  merchant_key      INT,
  merchant_id    	  CHAR(36),
  merchant_name  	  VARCHAR(150),
  address        	  VARCHAR(250),
  phone_number   	  VARCHAR(20),
  email          	  VARCHAR(100),
  ingestion_date 	  DATE
)
ORDER BY merchant_id
UNSEGMENTED ALL NODES;


-- STG.fact_payments_tmp
-- -----------------------

CREATE TABLE IF NOT EXISTS STG.fact_payments_tmp
(
  payment_id     	 INT,
  merchant_id    	 CHAR(36),
  payment_amount 	 NUMERIC(38,4),
  payment_method 	 VARCHAR(50),
  payment_status   VARCHAR(20),
  payment_date   	 TIMESTAMP,
  customer_id    	 CHAR(36),
  ingestion_date 	 DATE
)
SEGMENTED BY HASH(payment_id) ALL NODES;


-- STG.fact_payments
-- -----------------------

CREATE TABLE IF NOT EXISTS STG.fact_payments
(
  payment_id     	    INT,
  merchant_key    	  INT,
  payment_amount 	    NUMERIC(38,4),
  payment_method_key  INT,
  payment_status_key 	INT,
  payment_date_key   	INT NOT NULL,
  customer_id    	    CHAR(36),
  ingestion_date 	    DATE
)
ORDER BY payment_id, merchant_key
SEGMENTED BY HASH(payment_id) ALL NODES;


-- DWH
-- ------

CREATE SCHEMA IF NOT EXISTS DWH;


-- DWH.dim_time
-- -----------------------
CREATE TABLE IF NOT EXISTS DWH.dim_time
(
  date_key        INT NOT NULL PRIMARY KEY,
  full_date    	  DATE,
  day_of_week  	  VARCHAR(10),
  day_of_month    INT,
  day_of_year     INT,
  week_of_year    INT,
  quarter_of_year INT,
  day_of_quarte   INT
)
UNSEGMENTED ALL NODES;


-- insert 10 years of time
-- -----------------------
INSERT INTO DWH.dim_time
SELECT  REPLACE(LEFT(CAST(ts AS VARCHAR),10), '-', '')::INT AS date_key,
        ts::date                                            AS full_date,
        TO_CHAR(ts::date, 'Day')                            AS day_of_week,
        EXTRACT('Day' FROM ts::date)                        AS day_of_month,
        EXTRACT('DOY' FROM ts::date)                        AS day_of_year,
        EXTRACT('Week' FROM ts::date)                       AS week_of_year,
        EXTRACT('Quarter' FROM ts::date)                    AS quarter_of_year,
        EXTRACT('DOQ' FROM ts::date)                        AS day_of_quarter
FROM (SELECT '01-JAN-2018'::TIMESTAMP as tm UNION SELECT '01-JAN-2028'::TIMESTAMP as tm) as t
TIMESERIES ts as '1 DAY' OVER (ORDER BY tm);


-- DWH.dim_merchants
-- -----------------------

CREATE TABLE IF NOT EXISTS DWH.dim_merchants
(
  merchant_key     INT NOT NULL PRIMARY KEY,
  merchant_id    	 CHAR(36),
  merchant_name  	 VARCHAR(150),
  address        	 VARCHAR(250),
  phone_number   	 VARCHAR(20),
  email          	 VARCHAR(100),
  ingestion_date 	 DATE,
  dw_create_date   TIMESTAMP,
  dw_update_date   TIMESTAMP,
  ri_ind           BOOLEAN
)
ORDER BY merchant_id
UNSEGMENTED ALL NODES;



-- DWH.dim_payment_method
-- -----------------------

CREATE TABLE IF NOT EXISTS DWH.dim_payment_method
(
  payment_method_key  INT NOT NULL PRIMARY KEY,
  payment_method      VARCHAR(50),
  dw_create_date      TIMESTAMP,
  dw_update_date      TIMESTAMP,
  ri_ind              BOOLEAN
)
UNSEGMENTED ALL NODES;


-- DWH.dim_payment_status
-- -----------------------

CREATE TABLE IF NOT EXISTS DWH.dim_payment_status
(
  payment_status_key  INT NOT NULL PRIMARY KEY,
  payment_status      VARCHAR(50),
  dw_create_date      TIMESTAMP,
  dw_update_date      TIMESTAMP,
  ri_ind              BOOLEAN
)
UNSEGMENTED ALL NODES;


-- DWH.fact_payments
-- -----------------------

CREATE TABLE IF NOT EXISTS DWH.fact_payments
(
  payment_id     	    INT,
  merchant_key    	  INT,
  payment_amount 	    NUMERIC(38,4),
  payment_method_key  INT,
  payment_status_key 	INT,
  payment_date_key   	INT NOT NULL,
  customer_id    	    CHAR(36),
  ingestion_date 	    DATE,
  dw_create_date      TIMESTAMP,
  dw_update_date      TIMESTAMP,

  foreign key (payment_date_key)    references DWH.dim_time           (date_key),
  foreign key (merchant_key)        references DWH.dim_merchants      (merchant_key),
  foreign key (payment_method_key)  references DWH.dim_payment_method (payment_method_key),
  foreign key (payment_status_key)  references DWH.dim_payment_status (payment_status_key)
)
ORDER BY payment_id, merchant_key
SEGMENTED BY HASH(payment_id) ALL NODES
PARTITION BY((payment_date_key/100)::INT);